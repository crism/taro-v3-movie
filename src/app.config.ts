export default defineAppConfig({
  pages: ['pages/index/index', 'pages/login/index', 'pages/city/index', 'pages/cinema/index', 'pages/movie/index', 'pages/cinema-seat/index'],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  },
  lazyCodeLoading: 'requiredComponents',
  permission: {
    'scope.userLocation': {
      desc: '获取定位信息'
    }
  },
  subpackages: [
    {
      root: 'moduleA',
      pages: ['pages/order/index']
    }
    // {
    //   root: 'moduleB',
    //   pages: [],
    //   independent: true
    // }
  ]
})
