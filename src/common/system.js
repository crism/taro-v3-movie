import { setStorage, getStorageSync } from '@tarojs/taro'
export const getSysInfo = () => {
  let systemInfo = getStorageSync('SystemInfo')
  if (!systemInfo) {
    const rect = wx.getMenuButtonBoundingClientRect() //获取胶囊对象
    const { system, statusBarHeight, windowHeight, windowWidth, safeArea } = wx.getSystemInfoSync() // 获取设备系统对象
    systemInfo = {
      topBarH: 0,
      statusBarH: statusBarHeight,
      windowHeight,
      windowWidth,
      safeArea,
      menuButtonW: rect.width
    }
    if (system.toLowerCase().indexOf('ios') > -1) {
      systemInfo.topBarH = rect.bottom + (rect.top - statusBarHeight) * 2
    } else {
      systemInfo.topBarH = statusBarHeight + rect.height + (rect.top - statusBarHeight) * 2
    }
    setStorage({
      key: 'SystemInfo',
      data: systemInfo
    })
  }
  return systemInfo
}
