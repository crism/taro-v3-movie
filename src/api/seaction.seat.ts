import { request } from '@/common/request'

const getSeatsBySectionId = (data: any): Promise<any> => {
  return request({
    url: '/api/v1/movie-sectin/getSectionSeat',
    method: 'GET',
    data
  })
}

export { getSeatsBySectionId }
