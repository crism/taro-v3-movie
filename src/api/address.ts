import Taro from '@tarojs/taro'

const parseAddress = async ({ latitude, longitude }) => {
  const key = '5RWBZ-DOXL4-MH6UM-XJZE5-J22EE-4IBBW'
  let url = `https://apis.map.qq.com/ws/geocoder/v1/?location=${latitude},${longitude}&key=${key}&get_poi=1&poi_options=address_format=short;radius=10;policy=3`
  const { data, statusCode } = await Taro.request({ url })
  if (statusCode == 200) {
    const { address, location, formatted_addresses, pois, ad_info } = data && data.result
    const poiList: any = []
    pois.forEach((p: any) => {
      if (p._distance < 120 && !poiList.find((po: { _distance: any }) => po._distance == p._distance)) poiList.push(p)
    })
    return {
      pois: poiList,
      address,
      city: ad_info.city,
      recommend: formatted_addresses.recommend,
      ...location
    }
  }
  return {
    address: null,
    formatted_addresses: null,
    lat: null,
    lng: null
  }
}

export { parseAddress }
