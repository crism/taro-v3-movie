import { request } from '@/common/request'

const getAreaList = (): Promise<any> => {
  return request({
    url: '/api/v1/mini-common/area-list',
    method: 'GET'
  })
}

export { getAreaList }
