import { request } from '@/common/request'

const getTopics = (data: any): Promise<any> => {
  return request({
    url: 'api/v1/topics?page=1&tab=ask&limit=10',
    data: data,
    method: 'GET'
  })
}

export { getTopics }
