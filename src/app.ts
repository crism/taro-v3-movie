import { createApp } from 'vue'
import { Button, Elevator, Toast, Tabs, TabPane, Icon, SearchBar, InfiniteLoading } from '@nutui/nutui-taro'

import './app.less'
import './assets/font/iconfont.css'

const App = createApp({
  // onShow(options) {}
  // 入口组件不需要实现 render 方法，即使实现了也会被 taro 所覆盖
})

App.use(Button)
  .use(Toast)

  .use(Icon)
  .use(SearchBar)
  .use(TabPane)
  .use(Tabs)
  .use(InfiniteLoading)
  .use(Elevator)
export default App
