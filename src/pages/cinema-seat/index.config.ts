export default definePageConfig({
  navigationBarTitleText: '座位',
  navigationStyle: 'custom',
  disableScroll: false,
  enablePullDownRefresh: false
})
