export default definePageConfig({
  navigationBarTitleText: '影片详情',
  navigationStyle: 'custom',
  disableScroll: false,
  enablePullDownRefresh: false
})
