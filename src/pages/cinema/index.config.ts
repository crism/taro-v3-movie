export default definePageConfig({
  navigationBarTitleText: '影院',
  navigationStyle: 'custom',
  disableScroll: false,
  enablePullDownRefresh: false
})
